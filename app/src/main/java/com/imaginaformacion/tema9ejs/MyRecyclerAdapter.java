package com.imaginaformacion.tema9ejs;

import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import java.util.List;


public class MyRecyclerAdapter extends RecyclerView.Adapter<MyRecyclerAdapter.ViewHolder> {

    private List<Pelicula> items;
    private int itemLayout;
    private Context context;

    public MyRecyclerAdapter(Context context, List<Pelicula> items, int itemLayout) {
        this.context = context;
        this.items = items;
        this.itemLayout = itemLayout;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(itemLayout, parent, false);
        return new ViewHolder(v);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        final Pelicula item = items.get(position);

        holder.title.setText(item.getTitle());
        holder.genre.setText(item.getGenre());
        // if(item.getImage_url().length()>0) {

        Picasso.with(context).load(item.getImage_url()).into(holder.image);
        //}
        holder.info.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                context.startActivity(new Intent(context.getApplicationContext(), DetallePelicula.class).putExtra("id", item.getId()));
            }
        });
        holder.itemView.setTag(item);
    }


    public void add(Pelicula item, int position) {
        items.add(position, item);
        notifyItemInserted(position);
    }

    public void remove(Pelicula item) {
        int position = items.indexOf(item);
        items.remove(position);
        notifyItemRemoved(position);
    }

    @Override
    public int getItemCount() {
        return items.size();
    }

    public static class ViewHolder extends RecyclerView.ViewHolder {

        public ImageView image;
        public TextView title;
        public TextView genre;
        public Button info;

        public ViewHolder(View itemView) {
            super(itemView);

            image = (ImageView) itemView.findViewById(R.id.imageViewRow);
            title = (TextView) itemView.findViewById(R.id.textViewRowTitle);
            genre = (TextView) itemView.findViewById(R.id.textViewRowDescription);
            info = (Button) itemView.findViewById(R.id.buttonRowInfo);
        }
    }
}

package com.imaginaformacion.tema9ejs;


import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import java.util.ArrayList;
import java.util.List;

public class MySqlDTO {


    private final BdDao dao;

    public MySqlDTO(Context context) {
        //CONSTRUCTOR EL CUAL INSTANCIA AL DAO
        dao = new BdDao(context);
    }


    // Nombre de la tabla de Peliculas
    private static final String TABLE_PELICULAS = "peliculas";

    // Nombres de las columnas de la tabla de Peliculas
    private static final String KEY_ID = "id";
    private static final String KEY_TITLE = "title";
    private static final String KEY_GENRE = "genre";
    private static final String KEY_DURA = "dura";
    private static final String KEY_SIP = "sip";
    private static final String KEY_IMAGE_URL = "image_url";

    private static final String[] COLUMNS = {KEY_ID, KEY_TITLE, KEY_GENRE, KEY_SIP, KEY_DURA, KEY_IMAGE_URL};


    /**
     * Metodo para agregar una Pelicula
     *
     * @param pelicula
     */
    public void addPelicula(Pelicula pelicula) {

        SQLiteDatabase db = dao.getWritableDatabase();

        ContentValues values = new ContentValues();
        values.put(KEY_TITLE, pelicula.getTitle());
        values.put(KEY_GENRE, pelicula.getGenre());
        values.put(KEY_SIP, pelicula.getSipnosis());
        values.put(KEY_DURA, pelicula.getDura());
        values.put(KEY_IMAGE_URL, pelicula.getImage_url());

        db.insert(TABLE_PELICULAS, null, values);

        db.close();
    }


    /**
     * Para obtener una Pelicula, vaya un Select
     *
     * @param id
     * @return
     */
    public Pelicula getPelicula(int id) {

        Pelicula pelicula = null;

        SQLiteDatabase db = dao.getReadableDatabase();

        Cursor cursor = db.query(TABLE_PELICULAS, COLUMNS, "id = ?", new String[]{String.valueOf(id)}, null, null, null, null);
        if (cursor != null) {
            cursor.moveToFirst();
            pelicula = new Pelicula();

            pelicula.setTitle(cursor.getString(1));
            pelicula.setGenre(cursor.getString(2));
            pelicula.setSipnosis(cursor.getString(3));
            pelicula.setId(Integer.parseInt(cursor.getString(0)));
            pelicula.setDura(cursor.getString(4));
            pelicula.setImage_url(cursor.getString(5));
        }
        db.close();

        return pelicula;
    }

    public List<Pelicula> getAllPeliculas() {
        List<Pelicula> peliculas = new ArrayList<Pelicula>();

        SQLiteDatabase db = dao.getReadableDatabase();

        String query = ("SELECT * FROM " + TABLE_PELICULAS);
        Cursor cursor = db.rawQuery(query, null);

        /*String query2 =("SELECT * FROM ? WHERE id=?");
        Cursor b= db.rawQuery(query2,new String[]{TABLE_PELICULAS,"2"});*/
        Pelicula pelicula = null;

        if (cursor.moveToFirst()) {
            do {
                pelicula = new Pelicula();
                pelicula.setId(Integer.parseInt(cursor.getString(0)));
                pelicula.setTitle(cursor.getString(1));
                pelicula.setGenre(cursor.getString(2));
                pelicula.setSipnosis(cursor.getString(3));
                pelicula.setDura(cursor.getString(4));
                pelicula.setImage_url(cursor.getString(5));

                peliculas.add(pelicula);
            } while (cursor.moveToNext());

        }

        db.close();

        return peliculas;
    }

    public void updatePelicula(Pelicula pelicula) {

        SQLiteDatabase db = dao.getWritableDatabase();

        ContentValues values = new ContentValues();
        values.put("title", pelicula.getTitle());
        values.put("author", pelicula.getGenre());

        db.update(TABLE_PELICULAS,
                values,
                KEY_ID + " = ?",
                new String[]{String.valueOf(pelicula.getId())});


        db.close();


    }

    public void deletePelicula(Pelicula pelicula) {

        SQLiteDatabase db = dao.getWritableDatabase();

        db.delete(TABLE_PELICULAS,
                KEY_ID + " = ?",
                new String[]{String.valueOf(pelicula.getId())});

        db.close();


    }

    public void deleteAllPeliculas() {


        SQLiteDatabase db = dao.getWritableDatabase();

        db.execSQL("DELETE * FROM " + TABLE_PELICULAS);

        db.close();
    }
}
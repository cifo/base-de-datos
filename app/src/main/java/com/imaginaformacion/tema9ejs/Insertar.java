package com.imaginaformacion.tema9ejs;

import android.app.Activity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

public class Insertar extends Activity {
    EditText titulo, genero, sipnosis, duracion, url;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_insertar);

        titulo = (EditText) findViewById(R.id.titulo);
        genero = (EditText) findViewById(R.id.genero);
        sipnosis = (EditText) findViewById(R.id.etxt_sipnosis);
        duracion = (EditText) findViewById(R.id.etxt_dura);
        url = (EditText) findViewById(R.id.url);

        Button insertar = (Button) findViewById(R.id.btnAdd);
        insertar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                MySqlDTO db = new MySqlDTO(Insertar.this);

                db.addPelicula(new Pelicula(titulo.getText().toString(),
                        genero.getText().toString(),
                        sipnosis.getText().toString(),
                        duracion.getText().toString(),
                        url.getText().toString()));
                setResult(RESULT_OK);
                finish();
            }
        });
    }
}

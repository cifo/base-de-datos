package com.imaginaformacion.tema9ejs;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.TextView;

public class DetallePelicula extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detalle_pelicula);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);


        //Declaramos las referencias
        TextView titulo = (TextView) findViewById(R.id.detalle_title);
        TextView sipnosis = (TextView) findViewById(R.id.sinopsis);
        TextView duracion = (TextView) findViewById(R.id.detalle_duracion);
        TextView genero = (TextView) findViewById(R.id.detalle_genero);


        //Obtenemos el id de la pelicula que hemos realizado el click desde el adapter
        int idPelicula = getIntent().getExtras().getInt("id");
        //Generamos la consulta a la BD
        final MySqlDTO db = new MySqlDTO(this);
        final Pelicula seleccion = db.getPelicula(idPelicula);

        //Seteamos resultados

        titulo.setText(seleccion.getTitle());
        sipnosis.setText("SIPNOSIS\n" + seleccion.getSipnosis());
        duracion.setText("DURACION: " + seleccion.getDura());
        genero.setText("GENERO: " + seleccion.getGenre());


        final FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                db.deletePelicula(seleccion);
                Snackbar.make(view, "HAS ELIMINADO LA PELICULA", Snackbar.LENGTH_SHORT).show();
                Handler handler = new Handler();
                handler.postDelayed(new Runnable() {
                    public void run() {
                        startActivity(new Intent(DetallePelicula.this, MainActivity.class));
                    }
                }, 2500);


            }
        });
    }

}

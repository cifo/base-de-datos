package com.imaginaformacion.tema9ejs;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

/**
 * Created by manelcc on 04/11/2015.
 */
public class BdDao extends SQLiteOpenHelper {
    // Versión de la base de datos
    private static final int DATABASE_VERSION = 1;
    // Nombre de la base de datos
    private static final String DATABASE_NAME = "PeliculaDB";

    public BdDao(Context context) {

        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        // Sentencia SQL de creación de la tabla peliculas
        String CREATE_PELICULA_TABLE = "CREATE TABLE peliculas ( " +
                "id INTEGER PRIMARY KEY AUTOINCREMENT, " +
                "title TEXT, " +
                "genre TEXT, " +
                "sip TEXT, " +
                "dura TEXT, " +
                "image_url TEXT )";

        // Crear tabla "peliculas"
        db.execSQL(CREATE_PELICULA_TABLE);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        // Borrar tablas antiguas si existen
        db.execSQL("DROP TABLE IF EXISTS peliculas");

        // Crear tabla peliculas nueva
        this.onCreate(db);
    }
}
